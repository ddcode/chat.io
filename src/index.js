import express from 'express';
import http from 'http';
import SocketIO from 'socket.io';

const app = express();
const server = http.Server(app);
const io = new SocketIO(server);

const config = {
    port: 3000
}

app.use(express.static(__dirname + '/'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});


io.on('connection', socket => {
    console.log('a user connected');

    io.emit('connected', `New user connected`);

    socket.on('message', msg => {
        io.emit('exchange-messages', msg);
    })

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

server.listen(config.port, () => {
    console.log(`Listening on port: ${config.port}`);
});