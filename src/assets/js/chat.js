const randomInt = max => Math.floor(Math.random() * Math.floor(max));

const username = `Guest-${randomInt(9999)}`;

const socket = io();
const form = document.querySelector('form');
const input = document.querySelector('#message');
const messages = document.querySelector('#messages');

form.addEventListener('submit', e => {
    e.preventDefault();
    socket.emit('message', input.value);
    input.value = null;
})

socket.on('exchange-messages', msg => {
    const message = document.createElement('li');
    message.append(`${username}: ${msg}`);
    messages.append(message);
});

socket.on('connected', () => {
    const message = document.createElement('li')
    message.classList.add('new-user');

    message.append(`${username} connected`);
    messages.append(message);
});